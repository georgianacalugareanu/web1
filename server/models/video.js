'use strict';



module.exports = function (sequelize, DataTypes) {
    return sequelize.define('video', {
        title: DataTypes.STRING,
        channel: DataTypes.STRING,
        uploadDate: DataTypes.STRING,
        youtubeLink:DataTypes.STRING
    }, {
        underscored: true
    });

};