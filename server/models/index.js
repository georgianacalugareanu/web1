'use strict';
const connection = require('../conf/db');
const User = connection.import('./user');
const Feedback = connection.import('./feedback');
const Video = connection.import('./video');
const Playlist = connection.import('./playlist');


Playlist.belongsTo(User,{onDelete:'cascade'});

Playlist.hasMany(Video,{onDelete:'cascade'});

module.exports ={
  User,
  Video,
  Playlist,
  Feedback,
  connection
};
