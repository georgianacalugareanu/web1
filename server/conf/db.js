const Sequelize = require('sequelize');
const connection = new Sequelize('c9', 'root', '', {
  dialect: 'mysql',
  define: {
    timestamps: false
  }
});

module.exports = connection;