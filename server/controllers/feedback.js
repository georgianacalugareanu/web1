const Feedback = require('../models').Feedback;



module.exports.getFeedback = (req,res) =>{
    Feedback.findAll({where: {}, raw:true })
        .then((feedback) => {

             res.status(200).send(feedback);

        })
        .catch(() => res.status(500).send('hm, that was unexpected...'))
}


module.exports.postFeedback = (req,res) =>{
    Feedback.create({ name: req.body.name,
        text: req.body.text})
        .then(() => {

             res.status(201).send("created");

        })
        .catch(() => res.status(500).send('hm, that was unexpected...'))
}
