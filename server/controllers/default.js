const connection = require('../models').connection;
const User = require('../models').User;

module.exports.recreateTables = (req, res) => {
    connection.sync({force: true})
        .then(() =>
            User.create({
                userName: 'admin',
                password: 'admin',
                email: 'admin@admin.com'
            })
                .then(() => res.status(201).send('recreated all tables')))
        .catch(() => res.status(500).send('hm, that was unexpected...'))

}


