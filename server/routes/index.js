const express = require('express')
const router = express.Router();
const defaultController =require('../controllers/default')
const feedbackController =require('../controllers/feedback')
const userController =require('../controllers/user')

router.get('/', (req, res) => {
  res.status(200).send('Main Page');
});

router.get('/reset', defaultController.recreateTables);
router.get('/login', userController.login);

router.get('/feedback', feedbackController.getFeedback);
router.post('/feedback', feedbackController.postFeedback);

module.exports = router;